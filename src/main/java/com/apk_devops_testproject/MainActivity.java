package com.apk_devops_testproject;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText string_input = findViewById(R.id.StringInput);
        Button string_convert = findViewById(R.id.StringConvert);
        final TextView string_result = findViewById(R.id.StringResult);

        final EditText binary_input = findViewById(R.id.BinaryInput);
        Button binary_convert = findViewById(R.id.BinaryConvert);
        final TextView binary_result = findViewById(R.id.BinaryResult);

        string_convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                string_result.setText(stringToBinary(string_input.getText().toString()));
            }
        });

        binary_convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String bytes = binary_input.getText().toString();
                binary_result.setText(binaryToString(bytes));
            }
        });
    }

    public String stringToBinary(String input) {
        StringBuilder sb = new StringBuilder();
        char[] chars = input.toCharArray();
        for (char aChar : chars) {
            sb.append(String.format("%8s", Integer.toBinaryString(aChar)).replaceAll(" ", "0"));
        }
        return sb.toString();
    }

    public String binaryToString(String binaryString) {
        String result = "Wrong binary input";
        try {
            StringBuilder sb = new StringBuilder();
            int in;
            for (int i = 0; i < binaryString.length(); i += 8) {
                in = Integer.parseInt(binaryString.substring(i, i + 8), 2);
                String text = Character.toString((char) in);
                sb.append(text);
            }
            result = sb.toString();
        } catch (StringIndexOutOfBoundsException ignored) {
        }
        return result;
    }

}
