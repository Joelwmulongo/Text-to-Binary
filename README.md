# Text-to-Binary

Converts text to binary and reverse

```mermaid
graph LR;
    Text-->Binary;
    Binary-->Text;
```

<img src="https://user-images.githubusercontent.com/75514748/204720773-e11212e7-3ba0-4a2d-8996-7a16d69af7af.png">
